import logger
import network
from config import config
from worker.queue_worker import QueueWorker
import worker.queue_worker as queue_worker


class WifiMonitor(QueueWorker):
    @logger.log('WifiMonitor')
    def __init__(self, leds):
        self.leds = leds
        super().__init__()

    @logger.log('WifiMonitor')
    async def blink_for_status(self):
        status = network.WLAN(network.STA_IF).isconnected()
        if not status:
            await self.leds.red.blink(200, 10)
        return status

    @logger.log('WifiMonitor')
    async def check_status_and_queue_next_check(self):
        await self.queue_command(self.blink_for_status)
        await self.queue_command(self.check_status_and_queue_next_check)
        await queue_worker.sleep(config['wifi_monitor']['queue_check_interval'])

    @logger.log('WifiMonitor')
    def start_monitoring(self):
        self.queue_command_nowait(self.check_status_and_queue_next_check)
        return self.start()

