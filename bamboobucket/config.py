import json
import os

with open('config.json', 'r') as config_file:
    config = json.load(config_file)

with open('creds.json', 'r') as creds_file:
    config.update(json.load(creds_file))
