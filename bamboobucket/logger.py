import logging
from config import config


def setup_formatted_handler():
    handler = logging.StreamHandler()
    handler.setFormatter(setup_formatter())
    return handler


def setup_formatter():
    return logging.Formatter('%(name)s:%(levelname)s:%(message)s')


def setup_logger_logging(logger_name):
    logger = logging.getLogger(logger_name)
    logger.addHandler(setup_formatted_handler())
    logger.setLevel(config['logging_level'][logger_name])
    return logger


def setup_logging():
    for name in config['logging_level']:
        setup_logger_logging(name)


def log(logger_name):
    def log_decorator(func):
        def wrapper(*args, **kwargs):
            logger = logging.getLogger(logger_name)
            logger.info(func.__name__ + ": started")
            logger.debug(func.__name__ + ": started with: " + str(list(args[1:]))[1:-1])
            result = None
            try:
                result = func(*args, **kwargs)
                return result
            finally:
                logger.info(func.__name__ + ": completed")
                logger.debug(func.__name__ + ": completed with result: " + str(result))

        return wrapper

    return log_decorator
