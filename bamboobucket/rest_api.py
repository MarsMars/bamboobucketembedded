import tinyweb
import logging

from config import config

class RestApi:
    rest_api_app = tinyweb.webserver()
    def __init__(self):
        self.log = logging.getLogger('RestApi')
        self.log.debug(1)
        i = 0
        for endpoint_name in config['endpoints']:
            self.log.debug("2: %s" % endpoint_name)
            function_to_call = config['endpoints'][endpoint_name]

            function_to_define = f'''@rest_api_app.resource('/{str(endpoint_name)}')
async def endpoint_number_{i}(req):
    {function_to_call}()
    # await respo.start_html()
    # await respo.send("OK")
            '''
            print(function_to_define)
            exec(function_to_define)

            func_name = f"endpoint_{i + 1}_function"
            # print(f"Defined function: {func_name}")
            i += 1
        self.log.debug(3)

    def print_status(self):
        print("Status: OK")

    @rest_api_app.resource('/led/red/<led_state>')
    async def api_color(self, data, led_state):
        print(f"state, {led_state}")
        if led_state == "on":
            self.log.debug("turned on")
            #await current_loader.leds.red.on()
        if led_state == "off":
            self.log.debug("turned off")
            #await current_loader.leds.red.off()
        return {"OK"}

    @rest_api_app.route('/status')
    async def get_status(req, respo):
        print("checking status")
        # self.log.debug("checking status")
        await respo.start_html()
        await respo.send("OK")

    def rest_api_app_start(self):
        self.rest_api_app.run(host='0.0.0.0', port=8081, loop_forever=False)

