import logger
from config import config
import network
import logging


class WiFi:
    def __init__(self):
        self.gateway = network.WLAN(network.STA_IF)

    @logger.log('WiFi')
    def connect(self):
        log = logging.getLogger('wifi')
        self.gateway.active(True)
        self.gateway.connect(config['network_SSID'], config['network_key'])
        while not self.gateway.isconnected():
            pass
        log.info("WiFi connected to %s" % config['network_SSID'])
        log.info("IP address: %s" % self.gateway.ifconfig()[0])

    @logger.log('WiFi')
    def is_connected(self):
        return self.gateway.isconnected()
