import logger
import wifi
from config import config
from ledpanel import led_panel
from rest_api import RestApi
from wifi_monitor import WifiMonitor


class Loader:
    @logger.log('Loader')
    def __init__(self):
        self.leds = self.create_led_panel()
        self.connect_wifi()
        self.wifi_monitor = WifiMonitor(self.leds)
        # self.rest_api = RestApiLogging()

    @logger.log('Loader')
    def connect_wifi(self):
        wifi_network = wifi.WiFi()
        wifi_network.connect()

    @logger.log('Loader')
    def create_led_panel(self):
        return led_panel.LedPanel(config["leds"])
