import logging
import logger
from config import config
from loader import Loader
from machine import Pin, TouchPad
from rest_api import RestApi
from worker import queue_worker
import asyncio


try:
    import usocket as socket
except ImportError:
    import socket

logger.setup_logging()
log = logging.getLogger('main')


async def main(loader):
    log.info('Started')
    # loader.leds.start()
    # loader.rest_api.rest_api_app_start()
    # start_touch()
    await blink(6, loader)
    # the line below is an infinite loop
    await loader.wifi_monitor.start_monitoring()

    log.error('Completed')


async def touch():
    touch_pin = TouchPad(Pin(15, mode=Pin.IN))
    while True:
        touch_value = touch_pin.read()
        print(touch_value)
        await queue_worker.sleep(config['main']['main_sleep_time'])

def start_touch():
    log.info('Touch started')
    asyncio.create_task(touch())
    log.info('Touch completed')

async def blink(times, loader):
    log.info('Blink started')
    loader.leds.start()
    for i in range(times):
        await loader.leds.red.off()
        await loader.leds.blue.off()
        await loader.leds.green.off()
        await loader.leds.yellow.off()
        await queue_worker.sleep(100)
        await loader.leds.red.on()
        await loader.leds.blue.on()
        await loader.leds.green.on()
        await loader.leds.yellow.on()
        await queue_worker.sleep(100)
    log.info('Blink completed')

def start_blink(times):
    loader = Loader()
    asyncio.run(blink(times, loader))

def start_main():
    loader = Loader()
    asyncio.run(main(loader))

def start_rest_api():
    loader = Loader()
    rest_api_instance = RestApi()
    asyncio.run(rest_api_instance.rest_api_app_start())

if __name__ == '__main__':
    log.info('Booted')