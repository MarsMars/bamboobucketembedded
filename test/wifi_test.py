import unittest
from unittest.mock import patch
from bamboobucket import wifi


class WiFiSpecification(unittest.TestCase):
    @patch('network.WLAN.active')
    @patch('network.WLAN.connect')
    @patch('network.WLAN.isconnected')
    def test_network_functions_called_when_connect(self, mock_is_connected, mock_connect, mock_active):
        # GIVEN
        mock_is_connected.return_value = True
        wifi_network = wifi.WiFi()
        # WHEN
        wifi_network.connect()
        # THEN
        mock_active.assert_called_once()
        mock_connect.assert_called_once()
        mock_is_connected.assert_called()

    @patch('network.WLAN.isconnected')
    def test_is_connected_is_called_when_connection_checked(self, mock_is_connected):
        # GIVEN
        wifi_network = wifi.WiFi()
        # WHEN
        wifi_network.is_connected()
        # THEN
        mock_is_connected.assert_called_once()


if __name__ == '__main__':
    unittest.main()
