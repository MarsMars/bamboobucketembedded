import unittest
from unittest.mock import patch
from bamboobucket.loader import Loader
from config import config
from ledpanel.led_panel import LedPanel
from wifi_monitor import WifiMonitor


class LoaderSpecification(unittest.TestCase):
    def setUp(self) -> None:
        super().setUp()
        self.loader = Loader()

    def test_leds_set_when_loader_created(self):
        # WHEN
        loader = Loader()
        # THEN
        self.assertIsInstance(loader.leds, LedPanel)

    @patch('bamboobucket.loader.Loader.connect_wifi')
    def test_connects_wifi_when_created(self, mock_connect_wifi):
        # WHEN
        Loader()
        # THEN
        mock_connect_wifi.assert_called_once()

    @patch('wifi.WiFi')
    def test_network_created_when_wifi_connected(self, mock_network):
        # WHEN
        self.loader.connect_wifi()
        # THEN
        mock_network.assert_called_once()

    @patch('wifi.WiFi.connect')
    def test_network_connected_when_wifi_connected(self, mock_connect):
        # WHEN
        self.loader.connect_wifi()
        # THEN
        mock_connect.assert_called_once()

    def test_creates_wifi_monitor_when_created(self):
        self.assertIsInstance(self.loader.wifi_monitor, WifiMonitor)

    @patch('ledpanel.led_panel.LedPanel')
    def test_led_panel_created_when_create_led_panel_called(self, mock_panel):
        # WHEN
        self.loader.create_led_panel()
        # THEN
        mock_panel.assert_called_once_with(config["leds"])

    @patch('bamboobucket.loader.Loader.create_led_panel')
    def test_led_panel_created_when_loader_created(self, mock_create_led_panel):
        # WHEN
        Loader()
        # THEN
        mock_create_led_panel.assert_called_once()

    def test_leds_field_set_when_loader_created(self):
        # THEN
        self.assertIsInstance(self.loader.leds, LedPanel)


if __name__ == '__main__':
    unittest.main()
