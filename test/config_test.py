import unittest


class ConfigSpecification(unittest.TestCase):
    def test_network_configuration(self):
        from bamboobucket.config import config
        self.assertIsNotNone(config['network_SSID'])
        self.assertIsNotNone(config['network_key'])

    def test_logging_configuration(self):
        from bamboobucket.config import config
        self.assertIsNotNone(config['logging_level'])
        self.assertIsNotNone(config['logging_level'][''])
        self.assertIsNotNone(config['logging_level']['Loader'])


if __name__ == '__main__':
    unittest.main()
