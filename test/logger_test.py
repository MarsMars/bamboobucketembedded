import logging
import unittest
from unittest.mock import patch
import logger
from config import config


class LoggerSpecification(unittest.TestCase):
    @patch('logging.StreamHandler')
    def test_logger_stream_handler_retrieved_when_logging_setup(self, mock_stream_handler):
        # WHEN
        logger.setup_formatted_handler()

        # THEN
        mock_stream_handler.assert_called_once()

    @patch('logging.Formatter')
    def test_formatter_retrieved_when_logging_setup(self, mock_formatter):
        # WHEN
        logger.setup_formatted_handler()

        # THEN
        mock_formatter.assert_called_once_with('%(name)s:%(levelname)s:%(message)s')

    @patch('logging.Formatter')
    @patch('logging.StreamHandler.setFormatter')
    def test_formatter_set_when_logging_setup(self, mock_set_formatter, mock_formatter):
        # GIVEN
        mock_formatter.return_value = mock_formatter

        # WHEN
        logger.setup_formatted_handler()

        # THEN
        mock_set_formatter.assert_called_once_with(mock_formatter)

    def test_handler_returned_when_logging_setup(self):
        # WHEN
        handler = logger.setup_formatted_handler()

        # THEN
        self.assertIsInstance(handler, logging.Handler)

    def test_logger_returned_when_logging_setup(self):
        # WHEN
        log = logger.setup_logger_logging('Loader')

        # THEN
        self.assertIsInstance(log, logging.Logger)

    @patch('logging.getLogger')
    def test_get_logger_when_logging_setup(self, mock_get_logger):
        # WHEN
        logger.setup_logger_logging('Loader')

        # THEN
        mock_get_logger.assert_any_call('Loader')

    @patch('logging.Logger.addHandler')
    @patch('logger.setup_formatted_handler')
    def test_setup_formatted_handler_when_logging_setup(self, mock_formatted_handler, _):
        logger.setup_logger_logging('Loader')
        mock_formatted_handler.assert_called_once()

    @patch('logging.StreamHandler')
    @patch('logging.Logger.addHandler')
    def test_add_handler_when_logging_setup(self, mock_add_handler, mock_stream_handler):
        #GIVEN
        mock_stream_handler.return_value = mock_stream_handler

        # WHEN
        logger.setup_logger_logging('Loader')

        # THEN
        mock_add_handler.assert_called_once_with(mock_stream_handler)

    @patch.dict('config.config', {"logging_level": {"": "ERROR","Loader": "INFO"}})
    @patch('logger.setup_logger_logging')
    def test_logger_logging_when_logging_setup(self, mock_logger_logging):
        # WHEN
        logger.setup_logging()

        # THEN
        # mock_logger_logging.assert_called_once_with('')
        mock_logger_logging.assert_any_call('')
        mock_logger_logging.assert_any_call('Loader')

    @patch.dict('config.config', {"logging_level": {"": "ERROR","Loader": "INFO"}})
    @patch('logging.Logger.setLevel')
    def test_logging_level_when_logging_setup(self, mock_set_level):
        # WHEN
        logger.setup_logging()

        # THEN
        mock_set_level.assert_any_call('ERROR')
        mock_set_level.assert_any_call('INFO')

    @logger.log('Logger')
    def return_value(self, return_value, p1=1, p2="2", p3=None):
        return return_value

    @patch('logging.Logger.info')
    def test_log_decorator_logs_complete_message(self, mock_info):
        # GIVEN
        expected = config
        # WHEN
        result = self.return_value(expected)
        # THEN
        self.assertEqual(expected, result)
        mock_info.assert_any_call("return_value: completed")


    @patch('logging.Logger.debug')
    def test_log_decorator_logs_complete_message_with_return_value(self, mock_debug):
        # GIVEN
        expected = config
        # WHEN
        result = self.return_value(expected)
        # THEN
        self.assertEqual(expected, result)
        mock_debug.assert_any_call("return_value: completed with result: " + str(result))

    @patch('logging.Logger.info')
    def test_log_decorator_logs_start_message(self, mock_info):
        # GIVEN
        expected = config
        # WHEN
        result = self.return_value(expected, 1, "2", [3])
        # THEN
        self.assertEqual(expected, result)
        mock_info.assert_any_call("return_value: started")

    @patch('logging.Logger.debug')
    def test_log_decorator_logs_start_message_with_parameters(self, mock_debug):
        # GIVEN
        expected = config
        # WHEN
        result = self.return_value(expected, 1, "2", [3])
        # THEN
        self.assertEqual(expected, result)
        mock_debug.assert_any_call("return_value: started with: " + str(expected) + ", 1, '2', [3]")

    @patch('logging.getLogger')
    def test_proper_logger_is_used(self, mock_logger):
        # WHEN
        self.return_value(1)
        # THEN
        mock_logger.assert_called_once_with('Logger')


if __name__ == '__main__':
    unittest.main()
