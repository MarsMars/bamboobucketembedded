import unittest
from unittest.mock import patch
from unittest import IsolatedAsyncioTestCase
from bamboobucket import main
from loader import Loader


class MainTest(IsolatedAsyncioTestCase):

    @patch('asyncio.run')
    def test_start_main_calls_asyncio_run(self, mock_run):
        main.start_main()
        mock_run.assert_called_once()

    @patch('bamboobucket.main.main')
    def test_start_main_calls_main(self, mock_main):
        main.start_main()
        mock_main.assert_called_once()

    def test_logger_initialized(self):
        self.assertEqual('main', main.log.name)

    @patch('asyncio.sleep')
    @patch('logging.Logger.error')
    @patch('logging.Logger.info')
    async def test_main_log_started_and_completed(self, mock_info, mock_error, _):
        # GIVEN
        loader = Loader()
        await loader.wifi_monitor.finish()
        # WHEN
        await main.main(loader)
        # THEN
        mock_info.assert_any_call('Started')
        mock_error.assert_any_call('Completed')


if __name__ == '__main__':
    unittest.main()
