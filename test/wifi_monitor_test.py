import unittest
from unittest import IsolatedAsyncioTestCase, skip
from unittest.mock import patch, AsyncMock
from loader import Loader


class WifiMonitorSpecification(unittest.TestCase):
    def setUp(self):
        self.monitor = Loader().wifi_monitor

    @patch('wifi_monitor.WifiMonitor.start')
    @patch('wifi_monitor.WifiMonitor.queue_command_nowait')
    def test_queue_command_nowait_when_start_monitoring(self, mock_queue_command_nowait, _):
        # WHEN
        self.monitor.start_monitoring()
        # THEN
        mock_queue_command_nowait.assert_called_once_with(self.monitor.check_status_and_queue_next_check)

class WifiMonitorAsyncSpecification(IsolatedAsyncioTestCase):

    async def asyncSetUp(self) -> None:
        self.monitor = Loader().wifi_monitor
        return await super().asyncSetUp()

    @patch('worker.queue_worker.sleep')
    @patch('worker.queue_worker.QueueWorker.start')
    async def test_start_monitoring_starts_worker(self, mock_start, _):
        self.monitor.start_monitoring()
        mock_start.assert_called_once()

    @patch.dict('config.config', {"wifi_monitor": {"queue_check_interval": 10}})
    @patch('worker.queue_worker.sleep')
    @patch('wifi_monitor.WifiMonitor.queue_command', new_callable=AsyncMock)
    async def test_blink_and_wait_when_started(self, mock_queue_command, mock_sleep):
        # WHEN
        task = self.monitor.start_monitoring()
        self.monitor.queue_command_nowait(self.monitor.cancel)
        await task
        # THEN
        mock_queue_command.assert_any_call(self.monitor.blink_for_status)
        mock_sleep.assert_called_once_with(10)

    @patch('worker.queue_worker.sleep')
    @patch('network.WLAN.isconnected', side_effect=[True])
    @patch('ledpanel.led_thread.LedThread.blink', new_callable=AsyncMock)
    async def test_dont_blink_led_if_wifi_connected(self, mock_blink, _, __):
        self.assertTrue(await self.monitor.blink_for_status())
        self.assertEqual(0, mock_blink.call_count)

    @patch('network.WLAN.isconnected', side_effect=[False])
    @patch('ledpanel.led_thread.LedThread.blink', new_callable=AsyncMock)
    async def test_blink_three_times_red_led_if_wifi_disconnected(self, mock_blink, _):
        self.assertFalse(await self.monitor.blink_for_status())
        mock_blink.assert_called_with(200, 10)


if __name__ == '__main__':
    unittest.main()
