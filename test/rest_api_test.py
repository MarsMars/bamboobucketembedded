import unittest
from unittest import IsolatedAsyncioTestCase
from bamboobucket.rest_api import RestApi


class RestApiSpecification(unittest.TestCase):
    pass


class RestApiAsyncSpecification(IsolatedAsyncioTestCase):

    async def asyncSetUp(self) -> None:
        self.rest_api = RestApi()
        return await super().asyncSetUp()


if __name__ == '__main__':
    unittest.main()
