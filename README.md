# README

This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for?

* Quick summary
* Version

## How do I get set up?

### Initial WiFi connection

```python
import network

gateway = network.WLAN(network.STA_IF)
gateway.active(True)
gateway.connect('ssid', 'password')
gateway.isconnected()
```

### Dependencies

* Micropython
    * Support for queues in uasyncio is not yet in official build - thus you need to
      use [Micropython daily build](https://micropython.org/download/esp32/)
* Modules not included in Micropython builds
    * You need to install the modules below from ESP32 REPL. Make sure you are connected to the Internet first.
    ```
    import mip
    mip.install("logging")
    mip.install("abc")
    ```

* Micropython asyncio
    * Download Micropython Asyncio library
    ```commandline
    git clone https://github.com/peterhinch/micropython-async.git
    ```
    * Upload `v3/primitives` folder to ESP32. The easiest way for me was to use MicroPython plugin for PyCharm. I just
      had to mark the V3 directory as `Sources Root` so that the primitives fall into the proper folder on target.
    ```commandline
    ampy --port /dev/tty.usbserial-0001 put micropython-async/v3/primitives
    ```

* Tinyweb
  ```commandline
  git clone https://github.com/belyalov/tinyweb.git
  ampy --port /dev/ttyUSB0 put tinyweb/tinyweb
  ```

### WiFi configuration

* Create `creds.json` file in the bamboobucket directory with the following content: 

  ```json
  {
    "network_SSID": "SID",
    "network_key": "KEY"
  }
  ```  
* Replace "SID" and "KEY" with your WiFi credentials
* Upload `creds.json` to the target device:

  ```commandline
  ampy --port /dev/tty.usbserial-0001 put bamboobucket/creds.json
  ```

### Configuration

* All configurable parameters are in [config.json](./bamboobucket/config.json) file

### How to run tests

* Mark below directories as Source Root
* ![Scheme](images/SouceDirectories.png)
* Example configuration for all tests in bamboobucket directory
* ![Scheme](images/BambooBucketTestConfiguration.png)
* Create symbolic links

  ```commandline
  cd ~/bamboobucketembedded/test
  ln -s ../bamboobucket/config.json
  cd ~/bamboobucketembedded/bamboobucket
  ln -s ../mocks/network.py 
  ln -s ../mocks/machine.py 
  ```

### Interaction with files with USB

```commandline
ampy --port /dev/ttyUSB0 get "/server client echo.py" tymek.py
ampy --port /dev/ttyUSB0 ls
ls *.py *.json | xargs -n 1 ampy --port /dev/ttyUSB0 put
```

* If you encounter *Could not enter raw repl* error *ampy* may require a workaround with increasing
  sleep: https://github.com/scientifichackers/ampy/issues/19

### Connecting to board with USB

#### Mac

```commandline
screen /dev/tty.usbserial-0001 115200
```

* To detach from `screen` press `CRTL-a`+`d`
* To reconnect to the `screen` session you detached from

```commandline
$ screen -ls
There is a screen on:
	25867.ttys000.MKOLTOWS-M-FNV4	(Detached)
1 Socket in /var/folders/c4/dq7jf0_j34n3m8_v0vytnbnh0000gp/T/.screen.
$ screen -r 25867.ttys000.MKOLTOWS-M-FNV4
```

* To exit `screen` session entirely press `CRTL-a` + `k`

#### Linux

```commandline
picocom /dev/ttyUSB0 -b115200
```

* To exit `picocom`  press `CRTL-a`+`CRTL-x`

### Flashing micropython

#### Mac

```commandline
esptool.py --chip esp32 --port /dev/cu.usbserial-0001 erase_flash
esptool.py --chip esp32 --port /dev/cu.usbserial-0001 write_flash -z 0x1000 ./ESP32_GENERIC-20230426-v1.20.0.bin
```

#### Linux

```commandline
esptool.py --chip esp32 --port /dev/ttyUSB0 erase_flash
esptool.py --chip esp32 --port /dev/ttyUSB0 write_flash -z 0x1000 esp32-20210902-v1.17.bin
esptool.py --chip esp32 --port /dev/ttyUSB0 write_flash -z 0x1000 esp32-20211010-unstable-v1.17-74-gd42cba0d2.bin
```

#### Windows

```commandline
esptool.exe --chip esp32 --port COM5 erase_flash
esptool.exe --chip esp32 --port COM5 write_flash -z 0x1000 esp32-20211023-unstable-v1.17-93-g7e62c9707.bin
```

### Uploading project files

* Upload the entire folders to `/` on target:
    * `bamboobucket`
    * `ledpanel/ledpanel`
    * `web/web`
    * `worker/worker`
    * `tinyweb/tinyweb`
    ```commandline
    find bamboobucket -iname *.py -type f -o -iname *.json | xargs -n 1 ampy --port /dev/tty.usbserial-0001 put
    ampy --port /dev/tty.usbserial-0001 put ledpanel/ledpanel
    ampy --port /dev/tty.usbserial-0001 put web/web
    ampy --port /dev/tty.usbserial-0001 put worker/worker
    ampy --port /dev/tty.usbserial-0001 put tinyweb/tinyweb
    ```

## Micropython for PyCharm

* This plugin is especially useful for uploading entire folders to ESP32.

https://github.com/vlasovskikh/intellij-micropython#installation

## Contribution guidelines

* Writing tests
* Code review
* Other guidelines

## Who do I talk to?

* Repo owner or admin
* Other community or team contact