import logging

from worker.queue_worker import QueueWorker
import socket


class WebService(QueueWorker):
    default_address = ''
    default_port = 80
    socket_type = socket.SOCK_STREAM
    socket_family = socket.AF_INET

    def __init__(self, soc):
        super(WebService, self).__init__()
        self.socket = soc

    async def start_service(self):
        task = self.start()
        return task

    async def initialize_socket(self, address=default_address, port=default_port):
        self.socket.bind((address, port))
        self.socket.listen()

    async def get_request(self):
        conn, _ = self.socket.accept()
        return conn.recv(1024)


class WebServiceLogging(WebService):

    def __init__(self, soc):
        self.log = logging.getLogger('WebService')
        self.log.debug('Creating Web Service')
        super().__init__(soc)
        self.log.info('Web Service created')

    async def initialize_socket(self, address=WebService.default_address, port=WebService.default_port):
        await super().initialize_socket(address, port)
        self.log.info('Web Service initialized. Address: %s. Port: %s' % (address, port))

    async def start_service(self):
        task = await super().start_service()
        self.log.info('Web Service started')
        return task

    async def get_request(self):
        self.log.debug('Waiting for request')
        conn, addr = await super().get_request()
        self.log.debug('Request received: %s; %s' % (conn, addr))
        return conn, addr


# green_led_on = False

# def web_page():
#     global green_led_on
#     if green_led_on:
#         gpio_state = "ON"
#     else:
#         gpio_state = "OFF"
#     html = """<html><head> <title>ESP Web Server</title> <meta name="viewport" content="width=device-width, initial-scale=1">
#       <link rel="icon" href="data:,"> <style>html{font-family: Helvetica; display:inline-block; margin: 0px auto; text-align: center;}
#       h1{color: #0F3376; padding: 2vh;}p{font-size: 1.5rem;}.button{display: inline-block; background-color: #e7bd3b; border: none;
#       border-radius: 4px; color: white; padding: 16px 40px; text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}
#       .button2{background-color: #4286f4;}</style></head><body> <h1>ESP Web Server</h1>
#       <p>GPIO state: <strong>""" + gpio_state + """</strong></p><p><a href="/?led=on"><button class="button">ON</button></a></p>
#       <p><a href="/?led=off"><button class="button button2">OFF</button></a></p>
#       <p><a href="/?led=terminate"><button class="button button3">TERMINATE</button></a></p></body></html>"""
#     return html


# async def web_server():
#     global green_led_on
#     s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#     s.bind(('', 80))
#     s.listen(5)
#     i = 0
#
#     log.debug("1")
#     while i < 5:
#         log.debug("2")
#         try:
#             log.debug("3")
#             conn, addr = s.accept()
#             print('Got a connection from %s' % str(addr))
#             request = conn.recv(1024)
#             f = open("fixture%s.txt" % i, "w")
#             print("fixture%s.txt" % i)
#             i = i + 1
#             f.write(request)
#             print("Write")
#             f.close()
#             print("close")
#             request = str(request)
#             print('Content = %s' % request)
#             led_on = request.find('/?led=on')
#             led_off = request.find('/?led=off')
#             terminate = request.find('/?led=terminate')
#             if led_on == 6:
#                 green_led_on = True
#                 await loader.leds.green.on()
#             if led_off == 6:
#                 green_led_on = False
#                 await loader.leds.green.off()
#             if terminate == 6:
#                 raise Exception
#             response = web_page()
#             conn.send('HTTP/1.1 200 OK\n')
#             conn.send('Content-Type: text/html\n')
#             conn.send('Connection: close\n\n')
#             conn.sendall(response)
#             conn.close()
#             await asyncio.sleep(1)
#         except Exception as e:
#             print(e.message)