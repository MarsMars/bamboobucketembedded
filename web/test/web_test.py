import unittest
from unittest import IsolatedAsyncioTestCase
from unittest.mock import patch, ANY
from parameterized import parameterized

from web.web_service import WebService, WebServiceLogging
import socket


def get_test_socket():
    return socket.socket(WebService.socket_family, WebService.socket_type)


class WebServiceSpecification(IsolatedAsyncioTestCase):
    port = 10
    address_end = 1

    async def asyncSetUp(self) -> None:
        self.web = WebService(get_test_socket())
        await self.web.initialize_socket(address=self.get_any_not_used_address(), port=self.get_any_not_used_port())
        return await super().asyncSetUp()

    @staticmethod
    def get_any_not_used_port():
        WebServiceSpecification.port += 1
        return WebServiceSpecification.port

    @staticmethod
    def get_any_not_used_address():
        WebServiceSpecification.address_end += 1
        return '127.0.0.' + str(WebServiceSpecification.address_end)

    def test_remember_the_socket_it_was_created_with(self):
        # GIVEN
        soc = get_test_socket()
        # WHEN
        web = WebService(soc)
        # THEN
        self.assertEqual(web.socket, soc)

    @patch('worker.queue_worker.sleep')
    @patch('worker.queue_worker.QueueWorker.start')
    async def test_start_worker_when_started(self, mock_start, _):
        # WHEN
        await self.web.start_service()

        # THEN
        mock_start.assert_called_once()

    @patch('socket.socket.listen')
    @patch('socket.socket.bind')
    async def test_bind_socket_to_default_port_and_address_when_initialized(self, mock_bind, _):
        # WHEN
        await self.web.initialize_socket()

        # THEN
        mock_bind.assert_called_once_with((WebService.default_address, WebService.default_port))

    @patch('socket.socket.listen')
    @patch('socket.socket.bind')
    async def test_bind_socket_to_specific_port_when_initialized(self, mock_bind, _):
        # GIVEN
        port = 81

        # WHEN
        await self.web.initialize_socket(port=port)

        # THEN
        mock_bind.assert_called_once_with((ANY, port))

    @patch('socket.socket.listen')
    @patch('socket.socket.bind')
    async def test_bind_socket_to_specific_address_when_initialized(self, mock_bind, _):
        # GIVEN
        address = "127.0.0.1"

        # WHEN
        await self.web.initialize_socket(address=address)

        # THEN
        mock_bind.assert_called_once_with((address, ANY))

    @patch('socket.socket.bind')
    @patch('socket.socket.listen')
    async def test_listen_on_socket_when_initialized(self, mock_listen, _):
        # WHEN
        await self.web.initialize_socket()

        # THEN
        mock_listen.assert_called_once()

    @patch('socket.socket.recv')
    @patch('socket.socket.accept', side_effect=[(get_test_socket(), None)])
    async def test_accept_request_on_socket_when_asked(self, mock_accept, _):
        # WHEN
        await self.web.get_request()

        # THEN
        mock_accept.assert_called_once()

    @patch('socket.socket.accept', side_effect=[(get_test_socket(), None)])
    @patch('socket.socket.recv')
    async def test_retrieve_payload_when_message_received(self, mock_recv, _):
        # WHEN
        await self.web.get_request()

        # THEN
        mock_recv.assert_called_once_with(1024)

    # @parameterized.expand([[None, None]])
    @patch('socket.socket.accept', side_effect=[(get_test_socket(), None)])
    @patch('socket.socket.recv')
    # async def test_decodes_request_when_received(self, request, expected_action, mock_recv, _):
    async def test_decodes_request_when_received(self, mock_recv, _):
        # GIVEN
        request = open("empty_request_fixture.txt", "r").read()
        print()
        i = 0
        print('fixture%s.txt' % i)
        print('Content = %s' % request)
        print('Content = %s' % request.decode('utf-8'))
        mock_recv.return_value = request
        expected_action = None
        # WHEN
        action = await self.web.get_request()
        # THEN
        self.assertEqual(expected_action, action)


class WebServiceLoggingSpecification(IsolatedAsyncioTestCase):
    async def asyncSetUp(self) -> None:
        self.web = WebServiceLogging(get_test_socket())
        await self.web.initialize_socket(address=WebServiceSpecification.get_any_not_used_address(),
                                         port=WebServiceSpecification.get_any_not_used_port())
        return await super().asyncSetUp()

    @patch('logging.getLogger')
    def test_get_new_logger_when_created(self, mock_get_logger):
        # WHEN
        WebServiceLogging(get_test_socket())

        # THEN
        mock_get_logger.assert_any_call('WebService')

    @patch('logging.Logger.debug')
    def test_log_debug_when_creation_started(self, mock_debug):
        # WHEN
        WebServiceLogging(get_test_socket())

        # THEN
        mock_debug.assert_any_call('Creating Web Service')

    @patch('logging.Logger.info')
    def test_log_info_when_creation_finished(self, mock_info):
        # WHEN
        WebServiceLogging(get_test_socket())

        # THEN
        mock_info.assert_any_call('Web Service created')

    @patch('logging.Logger.info')
    async def test_log_initialization_info_with_address_and_port_when_initialized(self, mock_info):
        # GIVEN
        address = WebServiceSpecification.get_any_not_used_address()
        port = WebServiceSpecification.get_any_not_used_port()
        web = WebServiceLogging(get_test_socket())

        # WHEN
        await web.initialize_socket(address, port)

        # THEN
        mock_info.assert_any_call('Web Service initialized. Address: %s. Port: %s' % (address, port))

    @patch('worker.queue_worker.sleep')
    @patch('logging.Logger.info')
    async def test_log_starting_info_when_started(self, mock_info, _):
        # WHEN
        await self.web.start_service()

        # THEN
        mock_info.assert_any_call('Web Service started')

    @patch('worker.queue_worker.sleep')
    @patch('socket.socket.recv', side_effect=[(None, None)])
    @patch('socket.socket.accept', side_effect=[(get_test_socket(), None)])
    @patch('logging.Logger.debug')
    async def test_log_debug_when_waiting_for_request(self, mock_debug, mock_accept, _, __):
        # WHEN
        await self.web.get_request()

        # THEN
        mock_debug.assert_any_call('Waiting for request')

    @patch('worker.queue_worker.sleep')
    @patch('socket.socket.recv', side_effect=[("First value", "Second value")])
    @patch('socket.socket.accept', side_effect=[(get_test_socket(), None)])
    @patch('logging.Logger.debug')
    async def test_log_debug_when_request_received(self, mock_debug, _, __, ___):
        # WHEN
        await self.web.get_request()

        # THEN
        mock_debug.assert_any_call('Request received: %s; %s' % ("First value", "Second value"))


if __name__ == '__main__':
    unittest.main()
