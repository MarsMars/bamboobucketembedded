import logger


class WaterTank:
    @logger.log('WaterTank')
    def __init__(self, sensor):
        self.sensor = sensor

    @logger.log('WaterTank')
    def get_level(self):
        return self.sensor.read()
