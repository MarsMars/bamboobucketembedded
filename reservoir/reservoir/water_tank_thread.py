import logger
from config import config
from worker import queue_worker
from worker.queue_worker import QueueWorker


class WaterTankThread(QueueWorker):
    @logger.log('WaterTankThread')
    def __init__(self, tank, led):
        super().__init__()
        self.tank = tank
        self.led = led

    @logger.log('WaterTankThread')
    async def check_level_and_queue_next_check(self):
        await self.queue_command(self.get_level)
        await self.queue_command(self.check_level_and_queue_next_check)
        await queue_worker.sleep(config['tanks']['queue_check_interval'])

    @logger.log('WaterTankThread')
    async def get_level(self):
        self.tank.get_level()

    @logger.log('WaterTankThread')
    def start(self):
        self.queue_command_nowait(self.check_level_and_queue_next_check)
        return super().start()
