import unittest
from unittest import IsolatedAsyncioTestCase, skip
from unittest.mock import patch, AsyncMock

import machine
from config import config
from ledpanel.led_panel import LedPanel
from reservoir.water_tank import WaterTank
from reservoir.water_tank_thread import WaterTankThread


class WaterTankThreadSpecification(unittest.TestCase):
    def setUp(self):
        # GIVEN
        self.sensor = machine.TouchPad(machine.Pin(1, machine.Pin.IN))
        self.led = LedPanel(config["leds"]).yellow
        self.tank = WaterTankThread(self.sensor, self.led)

    def test_has_sensor_and_led_when_created(self):
        self.assertEqual(self.sensor, self.tank.tank)
        self.assertEqual(self.led, self.tank.led)

class WaterTankThreadAsyncSpecification(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        # GIVEN
        tank = WaterTank(machine.TouchPad(machine.Pin(config["tanks"]["first"], machine.Pin.IN)))
        led = LedPanel(config["leds"]).yellow
        self.tank = WaterTankThread(tank, led)

    @patch.dict('config.config', {"tanks": {"queue_check_interval": 10}})
    @patch('worker.queue_worker.sleep')
    @patch('reservoir.water_tank_thread.WaterTankThread.queue_command', new_callable=AsyncMock)
    async def test_get_level_queued_when_tank_thread_started(self, mock_queue_command, mock_sleep):
        # WHEN
        task = self.tank.start()
        self.tank.queue_command_nowait(self.tank.cancel)
        await task
        # THEN
        mock_queue_command.assert_any_call(self.tank.get_level)
        mock_sleep.assert_called_once_with(10)


    # @patch('reservoir.water_tank.WaterTank.get_level', new_callable=AsyncMock)