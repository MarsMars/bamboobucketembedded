import unittest
from unittest.mock import patch

import logger
import machine
from reservoir.water_tank import WaterTank


class WaterTankSpecification(unittest.TestCase):
    def __init__(self, method_name='runTest'):
        logger.setup_logging()
        super().__init__(method_name)

    def setUp(self):
        # GIVEN
        self.sensor = machine.TouchPad(machine.Pin(1, machine.Pin.IN))
        self.tank = WaterTank(self.sensor)

    def test_has_sensor_when_tank_created(self):
        self.assertEqual(self.sensor, self.tank.sensor)

    @patch('machine.TouchPad.read')
    def test_sensor_value_is_returned_when_level_read(self, mock_read):
        # GIVEN
        expected = 123
        mock_read.return_value = expected
        # WHEN
        actual = self.tank.get_level()
        # THEN
        self.assertEqual(expected, actual)


if __name__ == '__main__':
    unittest.main()
