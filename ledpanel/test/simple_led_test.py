import unittest
from unittest import IsolatedAsyncioTestCase
from unittest.mock import patch
from ledpanel.simple_led import SimpleLed
from bamboobucket.machine import Pin


class SimpleLedSpecification(unittest.TestCase):
    def __init__(self, method_name='runTest'):
        self.pin = Pin(1, Pin.OUT)
        self.color = 'red'
        self.led = SimpleLed(self.color, self.pin)
        super().__init__(method_name)

    def test_led_color_setting(self):
        self.assertEqual(self.color, self.led.get_color())


class SimpleLedAsyncSpecification(IsolatedAsyncioTestCase):
    def __init__(self, method_name='runTest'):
        self.pin = Pin(1, Pin.OUT)
        self.color = 'red'
        super().__init__(method_name)

    async def asyncSetUp(self) -> None:
        self.led = SimpleLed(self.color, self.pin)
        return await super().asyncSetUp()

    @patch('bamboobucket.machine.Pin.on')
    async def test_led_on(self, mock_on):
        await self.led.on()
        mock_on.assert_called_once()

    @patch('bamboobucket.machine.Pin.off')
    async def test_led_off(self, mock_off):
        await self.led.off()
        mock_off.assert_called_once()


if __name__ == '__main__':
    unittest.main()
