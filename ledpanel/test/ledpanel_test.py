import unittest
from unittest.mock import patch
import logger
from ledpanel.led_panel import LedPanel
from machine import Pin
from ledpanel.led_thread import LedThread
from ledpanel.simple_led import SimpleLed

pin_out = Pin.OUT

class LedPanelSpecification(unittest.TestCase):
    def __init__(self, method_name='runTest'):
        logger.setup_logging()
        super().__init__(method_name)

    def setUp(self):
        self.leds_config = {'red': 0, 'green': 1, 'blue': 2}
        self.led_panel = LedPanel(self.leds_config)

    @patch('ledpanel.led_panel.LedPanel._create_pins')
    def test_creates_pins(self, mock_create_pins):
        # WHEN
        self.led_panel._create_pins(self.leds_config)
        # THEN
        mock_create_pins.assert_called_once_with(self.leds_config)

    def test_create_pins_creates_all_pins(self):
        # WHEN
        pins = self.led_panel._create_pins(self.leds_config)
        # THEN
        for pin in pins:
            self.assertIsInstance(pin, Pin)
        self.assertEqual(len(self.leds_config), len(pins))

    @patch('machine.Pin')
    def test_create_pins_creates_pin_with_proper_numbers_and_type(self, mock_pin):
        mock_pin.OUT = pin_out
        # WHEN
        pins = self.led_panel._create_pins(self.leds_config)
        for color in self.leds_config:
            mock_pin.assert_any_call(self.leds_config[color], pin_out)

    @patch('machine.Pin.off')
    def test_turning_pins_off(self, mock_pin_off):
        # GIVEN
        pins = self.led_panel._create_pins(self.leds_config)
        # WHEN
        self.led_panel._turn_pins_off(pins)
        self.assertEqual(len(pins), mock_pin_off.call_count)

    @patch('ledpanel.led_panel.LedPanel._turn_pins_off')
    def test_pins_created_and_turned_off_when_creating_turned_off_pins(self, mock_pin_turned_off):
        # WHEN
        pins = self.led_panel._create_turned_off_pins(self.leds_config)
        # THEN
        mock_pin_turned_off.assert_called_once_with(pins)

    @patch('ledpanel.led_panel.LedPanel._create_turned_off_pins')
    def test_pins_created_when_leads_created(self, mock_create_turned_off_pins):
        # GIVEN
        mock_create_turned_off_pins.return_value = list(range(len(self.leds_config)))
        # WHEN
        self.led_panel._create_leds(self.leds_config)
        # THEN
        mock_create_turned_off_pins.assert_called_once_with(self.leds_config)


    @patch('ledpanel.led_panel.LedPanel._create_leds_from_pins')
    def test_leds_from_pins_created_when_leds_created(self, mock_create_leds_from_pins):
        # WHEN
        self.led_panel._create_leds(self.leds_config)
        # THEN
        mock_create_leds_from_pins.assert_called_once()

    @patch('ledpanel.simple_led.SimpleLed')
    def test_proper_pins_are_used_when_leds_created(self, mock_led):
        # GIVEN
        pins = self.led_panel._create_turned_off_pins(self.leds_config)
        # WHEN
        self.led_panel._create_leds_from_pins(self.leds_config, pins)
        #THEN
        i = iter(pins)
        for color in self.leds_config:
            mock_led.assert_any_call(color, next(i))

    def test_proper_number_of_leds_created_when_leds_created(self):
        # WHEN
        leds = self.led_panel._create_leds(self.leds_config)
        # THEN
        self.assertEqual(len(leds), len(self.leds_config))
        for led in leds:
            self.assertIsInstance(led, SimpleLed)

    def test_leds_threads_created_when_threads_created(self):
        # GIVEN
        leds = self.led_panel._create_leds(self.leds_config)
        # WHEN
        threads = self.led_panel._create_threads(leds)
        # THEN
        self.assertEqual(len(self.leds_config), len(threads))
        for thread in threads:
            self.assertIsInstance(thread, LedThread)

    def test_leds_fields_defined_when_threads_created(self):
        # GIVEN
        leds = self.led_panel._create_leds(self.leds_config)
        # WHEN
        self.led_panel._create_threads(leds)
        # THEN
        for color in self.leds_config:
            self.assertIsInstance(getattr(self.led_panel, color), LedThread)

    @patch('ledpanel.led_panel.LedPanel._create_threads')
    def test_threads_created_when_panel_created(self, mock_create_threads):
        # WHEN
        LedPanel(self.leds_config)
        # THEN
        mock_create_threads.assert_called_once()

    @patch('worker.queue_worker.QueueWorker.start')
    def test_worker_started_for_all_threads_when_panel_started(self, mock_start):
        # WHEN
        self.led_panel.start()
        # THEN
        self.assertEqual(len(self.leds_config), mock_start.call_count)


if __name__ == '__main__':
    unittest.main()
