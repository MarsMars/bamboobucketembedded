import unittest
from unittest.mock import patch, AsyncMock
from unittest import IsolatedAsyncioTestCase
from ledpanel.led_thread import LedThread
import logger
from ledpanel.simple_led import SimpleLed
from bamboobucket.machine import Pin


class LedThreadTest(IsolatedAsyncioTestCase):
    def __init__(self, method_name='runTest'):
        logger.setup_logging()
        super().__init__(method_name)

    async def asyncSetUp(self) -> None:
        self.led = SimpleLed('blue', Pin(1, Pin.OUT))
        self.thread = LedThread(self.led)
        return await super().asyncSetUp()

    async def test_is_thread_initialized(self):
        self.assertEqual(self.led, self.thread.led)

    def test_led_get_color(self):
        self.assertEqual(self.led.get_color(), self.thread.get_color())

    @patch('ledpanel.led_thread.LedThread.do_work', new_callable=AsyncMock)
    async def test_starting_task(self, mock_do_work):
        self.thread.start()
        mock_do_work.assert_called_once()

    @patch('asyncio.Queue.put')
    async def test_led_on_sends_command_to_queue(self, mock_put):
        await self.thread.on()
        mock_put.assert_called_once_with(self.thread.led.on)


class LedThreadTaskTest(IsolatedAsyncioTestCase):
    async def asyncSetUp(self) -> None:
        self.led = SimpleLed('blue', Pin(1, Pin.OUT))
        self.thread = LedThread(self.led)
        self.task = self.thread.start()
        return await super().asyncSetUp()

    async def asyncTearDown(self) -> None:
        await self.thread.finish()
        return await super().asyncTearDown()

    @patch('ledpanel.simple_led.SimpleLed.off', new_callable=AsyncMock)
    async def test_off_command_sent_to_queue_is_executed_by_worker(self, mock_off):
        await self.thread.off()
        await self.thread.finish()
        await self.task
        mock_off.assert_called_once()

    @patch('ledpanel.simple_led.SimpleLed.off', new_callable=AsyncMock)
    @patch('ledpanel.led_thread.sleep')
    @patch('ledpanel.simple_led.SimpleLed.on', new_callable=AsyncMock)
    async def test_on_for(self, mock_on, mock_sleep, mock_off):
        await self.thread.on_for(100)
        await self.thread.finish()
        await self.task
        mock_on.assert_called_once()
        mock_sleep.assert_called_once_with(100)
        mock_off.assert_called_once()

    @patch('ledpanel.simple_led.SimpleLed.off', new_callable=AsyncMock)
    @patch('ledpanel.led_thread.sleep')
    @patch('ledpanel.simple_led.SimpleLed.on', new_callable=AsyncMock)
    async def test_blink(self, mock_on, mock_sleep, mock_off):
        time = 100
        times = 5
        await self.thread.blink(time, times)
        await self.thread.finish()
        await self.task
        mock_sleep.assert_any_call(time)
        self.assertEqual(2 * times, mock_sleep.call_count)
        self.assertEqual(times, mock_on.call_count)
        self.assertEqual(times, mock_off.call_count)

    @patch('ledpanel.simple_led.SimpleLed.off', new_callable=AsyncMock)
    @patch('ledpanel.simple_led.SimpleLed.on', new_callable=AsyncMock)
    async def test_turn_on_and_off(self, mock_on, mock_off):
        await self.thread.on()
        await self.thread.off()
        await self.thread.finish()
        await self.task
        mock_on.assert_called_once()
        mock_off.assert_called_once()


if __name__ == '__main__':
    unittest.main()
