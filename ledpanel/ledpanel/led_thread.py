import logger
from worker.queue_worker import QueueWorker, sleep
from ledpanel.single_led import SingleLed


class LedThread(QueueWorker):

    @logger.log('LedThread')
    def __init__(self, led: SingleLed):
        super().__init__()
        self.led = led

    @logger.log('LedThread')
    def get_color(self):
        return self.led.get_color()

    @logger.log('LedThread')
    async def on(self):
        await self.queue_command(self.led.on)

    @logger.log('LedThread')
    async def off(self):
        await self.queue_command(self.led.off)

    @logger.log('LedThread')
    async def on_for(self, time):
        await self.on()
        await sleep(time)
        await self.off()

    @logger.log('LedThread')
    async def blink_once(self, time):
        await self.on()
        await sleep(time)
        await self.off()
        await sleep(time)

    @logger.log('LedThread')
    async def blink(self, time, times):
        for _ in range(times):
            await self.blink_once(time)
