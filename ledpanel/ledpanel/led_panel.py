import logger
import machine
from ledpanel import simple_led
from ledpanel.led_thread import LedThread


class LedPanel:
    @logger.log('LedPanel')
    def __init__(self, leds_config):
        leds = self._create_leds(leds_config)
        self.threads = self._create_threads(leds)

    @logger.log('LedPanel')
    def _create_pins(self, leds_config):
        pins = []
        for color in leds_config:
            pins.append(machine.Pin(leds_config[color], machine.Pin.OUT))
        return pins

    @logger.log('LedPanel')
    def _turn_pins_off(self, pins):
        for pin in pins:
            pin.off()

    @logger.log('LedPanel')
    def _create_turned_off_pins(self, leds_config):
        pins = self._create_pins(leds_config)
        self._turn_pins_off(pins)
        return pins

    @logger.log('LedPanel')
    def _create_leds_from_pins(self, leds_config, pins):
        leds = []
        pin = iter(pins)
        for color in leds_config:
            leds.append(simple_led.SimpleLed(color, next(pin)))
        return leds

    @logger.log('LedPanel')
    def _create_leds(self, leds_config):
        pins = self._create_turned_off_pins(leds_config)
        leds = self._create_leds_from_pins(leds_config, pins)
        return leds

    @logger.log('LedPanel')
    def _create_threads(self, leds):
        threads = self._create_threads_from_leds(leds)
        self._add_fields_for_threads_by_color(threads)
        return threads

    @logger.log('LedPanel')
    def _create_threads_from_leds(self, leds):
        threads = []
        for led in leds:
            threads.append(LedThread(led))
        return threads

    @logger.log('LedPanel')
    def _add_fields_for_threads_by_color(self, threads):
        for thread in threads:
            setattr(self, thread.led.get_color(), thread)

    @logger.log('LedPanel')
    def start(self):
        for thread in self.threads:
            thread.start()
