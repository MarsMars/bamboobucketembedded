import abc


class SingleLed:
    @abc.abstractmethod
    def __init__(self, color, pin):
        raise NotImplementedError

    @abc.abstractmethod
    async def on(self):
        raise NotImplementedError

    @abc.abstractmethod
    async def off(self):
        raise NotImplementedError

    @abc.abstractmethod
    def get_color(self):
        raise NotImplementedError
