import logger
from ledpanel.single_led import SingleLed


class SimpleLed(SingleLed):

    @logger.log('SimpleLed')
    def __init__(self, color, pin):
        self.__color = color
        self.pin = pin

    @logger.log('SimpleLed')
    async def on(self):
        self.pin.on()

    @logger.log('SimpleLed')
    async def off(self):
        self.pin.off()

    @logger.log('SimpleLed')
    def get_color(self):
        return self.__color

