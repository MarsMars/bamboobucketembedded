import unittest
from unittest import IsolatedAsyncioTestCase
from unittest.mock import patch
import logger
from worker.queue_worker import QueueWorker
from asyncio import CancelledError


class QueueWorkerSpecification(unittest.TestCase):
    def test_create_infinite_worker(self):
        worker = QueueWorker()
        self.assertFalse(worker.is_finished())

    def test_create_finite_worker(self):
        worker = QueueWorker(run_forever=False)
        self.assertTrue(worker.is_finished())


class QueueWorkerAsyncSpecification(IsolatedAsyncioTestCase):

    def __init__(self, method_name='runTest'):
        logger.setup_logging()
        super().__init__(method_name)

    async def async_command(self):
        self.async_called = True

    async def asyncSetUp(self) -> None:
        self.async_called = False
        self.worker = QueueWorker()
        return await super().asyncSetUp()

    async def test_start_can_create_task_only_once(self):
        # GIVEN
        worker = QueueWorker()
        # WHEN
        worker.start()
        task1 = worker.task
        worker.start()
        # THEN
        self.assertEqual(task1, worker.task)

    async def test_async_command_call(self):
        await self.worker.queue_command(self.async_command)
        await self.worker.finish()
        await self.worker.do_work()
        self.assertTrue(self.async_called)

    async def test_async_command_call_nowait(self):
        self.worker.queue_command_nowait(self.async_command)
        await self.worker.finish()
        await self.worker.do_work()
        self.assertTrue(self.async_called)

    async def test_cancel_throws_exception(self):
        with self.assertRaises(CancelledError):
            await QueueWorker().cancel()
        await self.worker.finish()
        await self.worker.do_work()


class QueueWorkerLoggingSpecification(IsolatedAsyncioTestCase):
    async def async_command(self):
        self.async_called = True

    async def asyncSetUp(self) -> None:
        self.worker = QueueWorker()
        return await super().asyncSetUp()

    @patch('logging.Logger.debug')
    async def test_execute_commands_from_queue(self, mock_debug):
        await self.worker.finish()
        await self.worker.queue_command(self.async_command)
        await self.worker.do_work()
        mock_debug.assert_any_call('queue_command: number of commands in the queue: %d', 2)


if __name__ == '__main__':
    unittest.main()
