import asyncio
import logging
from asyncio import CancelledError
import logger


class QueueWorker:

    @logger.log('QueueWorker')
    def __init__(self, run_forever=True):
        self.run_forever = run_forever
        self.task = None
        try:
            self.queue = asyncio.Queue()
        except AttributeError:
            import primitives.queue as q
            self.queue = q.Queue()

    @logger.log('QueueWorker')
    def start(self):
        if self.task is None:
            self.task = asyncio.create_task(self.do_work())
        return self.task

    @logger.log('QueueWorker')
    async def do_work(self):
        try:
            await self.execute_commands_from_queue()
        except CancelledError:
            pass

    @logger.log('QueueWorker')
    async def execute_commands_from_queue(self):
        while not self.is_finished():
            command = await self.queue.get()
            await self.execute_command(command)

    @logger.log('QueueWorker')
    def is_finished(self):
        if self.run_forever:
            return False
        else:
            return self.queue.empty()

    @logger.log('QueueWorker')
    async def execute_command(self, command):
        await command()

    @logger.log('QueueWorker')
    async def queue_command(self, command):
        await self.queue.put(command)
        logging.getLogger('QueueWorker').debug('queue_command: number of commands in the queue: %d', self.queue.qsize())

    @logger.log('QueueWorker')
    async def cancel(self):
        raise CancelledError()

    @logger.log('QueueWorker')
    async def finish(self):
        await self.queue_command(self.cancel)

    @logger.log('QueueWorker')
    def queue_command_nowait(self, command):
        self.queue.put_nowait(command)
        logging.getLogger('QueueWorker').debug('queue_command_nowait: number of commands in the queue: %d', self.queue.qsize())


def get_command_name(command):
    try:
        return command.__name__
    except AttributeError:
        return command.name


async def sleep(time):
    try:
        await asyncio.sleep_ms(time)
    except AttributeError:
        await asyncio.sleep(time)
